package com.aigao.golf

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication


@SpringBootApplication
class GolfApplication

fun main(args: Array<String>) {
    SpringApplication.run(GolfApplication::class.java, *args)
}
