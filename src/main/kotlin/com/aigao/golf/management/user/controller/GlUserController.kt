package com.aigao.golf.management.user.controller

import com.aigao.golf.common.base.BaseLoginController
import com.aigao.golf.common.result.ResultTo
import com.aigao.golf.common.shiro.MyUPToken
import com.aigao.golf.management.user.model.GlUser
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Created by gelon on 2017/12/9.
 * 系统用户登录
 */
@RestController
@RequestMapping("/managerment/user")
class GlUserController : BaseLoginController<GlUser>() {

    @PostMapping("/login")
    fun login(username: String, password: String): ResultTo {
        val token = MyUPToken(username, password, MyUPToken.GL_WEB)
        return validateLogin(token)
    }


    @PostMapping("/test")
    fun test():ResultTo = ResultTo()

}
