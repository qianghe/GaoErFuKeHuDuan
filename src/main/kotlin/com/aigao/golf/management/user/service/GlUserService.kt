package com.aigao.golf.management.user.service

import com.aigao.golf.common.base.MyBaseService
import com.aigao.golf.management.user.model.GlUser


/**
 * @author gelon
 * createTime 2017/12/09
 */
interface GlUserService:MyBaseService<GlUser>
