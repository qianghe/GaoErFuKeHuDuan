package com.aigao.golf.management.user.model

import com.aigao.golf.common.CommonUser
import com.aigao.golf.common.base.BaseModel
import java.util.Date
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

/**
 * @author gelon createTime 2017/12/09
 */
@Table(name = "GL_USER")
class GlUser : BaseModel<String>(), CommonUser {

    /**
     *
     */
    @Id
    @GeneratedValue(generator="UUID")
    override var id: String? = null
    /**
     * 用戶名
     */
    var username: String? = null

    /**
     * 手机号
     */
    override var phone: String? = null
    /**
     * 授权码
     */
    override var authorizationCode: String? = null


    /**
     * 密碼
     */
    var password: String? = null
    /**
     * 名称
     */
    override var cname: String? = null
    /**
     * 盐
     */
    var salt: String? = null
    /**
     * 创建者
     */
    override var creater: String? = null
    /**
     * 创建时间
     */
    override var createTime: Date? = null
    /**
     * 机构
     */
    override var orgId: String? = null
}
