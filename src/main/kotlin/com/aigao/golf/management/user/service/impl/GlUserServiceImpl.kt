package com.aigao.golf.management.user.service.impl

import com.aigao.golf.common.base.MyBaseMapper
import com.aigao.golf.common.base.MyBaseServiceImpl
import com.aigao.golf.management.user.dao.GlUserMapper
import com.aigao.golf.management.user.model.GlUser
import com.aigao.golf.management.user.service.GlUserService

/**
 * @author gelon
 * createTime 2017/12/09
 */
@org.springframework.stereotype.Service
class GlUserServiceImpl : MyBaseServiceImpl<GlUser>(),GlUserService {


    @org.springframework.beans.factory.annotation.Autowired
    private lateinit var glUserMapper: GlUserMapper
}
