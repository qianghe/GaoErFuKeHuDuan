package com.aigao.golf.management.user.controller

import com.aigao.golf.common.result.ResultTo
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/more")
class MoreController {


    @PostMapping("/test")
    fun test(): ResultTo {
        return ResultTo()
    }

}
