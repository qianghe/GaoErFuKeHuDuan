package com.aigao.golf.management.user.dao

import com.aigao.golf.common.base.MyBaseMapper
import com.aigao.golf.management.user.model.GlUser

/**
 * @author gelon
 * createTime 2017/12/09
 */
interface GlUserMapper : MyBaseMapper<GlUser>
