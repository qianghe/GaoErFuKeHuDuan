package com.aigao.golf.exts

import com.aigao.golf.Utils.IDCardUtil
import com.aigao.golf.common.Rex
import java.util.regex.Pattern

    /**
     * 通用验证
     */
    fun String.valid(regex: String): Boolean {
        val pattern = Pattern.compile(regex)
        val matcher = pattern.matcher(this)
        return matcher.matches()
    }

    /**
     * 验证身邮箱
     * @param value
     * @return
     */
    fun String.validEmail(): Boolean {
        return this.valid(Rex.REGEX_EMAIL)
    }

    /**
     * 验证手机号
     *
     * @param value
     * @return
     */
    fun String.validPhone(): Boolean {
        return this.valid( Rex.REGEX_PHONE)
    }

    /**
     * 验证身份证
     *
     * @param value
     * @return
     */
    fun String.validIdCard(): Boolean {
        return IDCardUtil.verify(this)
    }

    /**
     * 验证Json
     *
     * @param value
     * @return
     */
    fun String.validJson(): Boolean {
        return this.valid( Rex.REGEX_JSON)
    }

    /**
     * 验证用户名
     *
     * @param value
     * @return
     */
    fun String.validUserName(value: String): Boolean {
        return this.valid(Rex.REGEX_USERNAME)
    }

    /**
     * 验证URL
     *
     * @param value
     * @return
     */
    fun String.validUrl(): Boolean {
        return this.valid( Rex.REGEX_URL)
    }

    /**
     * 验证Ip
     *
     * @param value
     * @return
     */
    fun String.validIp(value: String): Boolean {
        return this.valid(Rex.REGEX_URL)
    }
