package com.aigao.golf.common.file.fastDFS

import com.aigao.golf.common.file.FileLoadBean
import com.aigao.golf.common.file.IFileManager
import com.aigao.golf.common.file.IFileService
import com.aigao.golf.common.file.fastDFS.manager.FastDFSFile
import com.aigao.golf.common.file.fastDFS.pool.ConnectionPool
import org.apache.commons.lang3.StringUtils
import org.csource.common.MyException
import org.csource.common.NameValuePair
import org.csource.fastdfs.StorageClient
import org.csource.fastdfs.StorageServer
import org.csource.fastdfs.TrackerServer
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.multipart.MultipartFile
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import java.util.ArrayList
import java.util.Base64
import kotlin.text.Charsets.ISO_8859_1

class FastDFSService : IFileManager {

    lateinit var fileService: IFileService

    lateinit var connectionPool: ConnectionPool
    lateinit var fastDfsService: String
    lateinit var author: String

    fun init(): StorageClient {
        if (StringUtils.isBlank(fastDfsService)) {
            log.warn("fastDfsService为空")
        }
        trackerServer = connectionPool.checkout()
        return StorageClient(trackerServer, storageServer)
    }

    override fun upload(file: MultipartFile): FileLoadBean? {
        return upload(null, file)
    }

    override fun upload(groupName: String?, file: MultipartFile): FileLoadBean? {
        if (file.size > 0) {
            val ext = file.originalFilename.substring(file.originalFilename.lastIndexOf(".") + 1)
            try {
                val fastDFSFile = FastDFSFile(file.bytes, ext)
                val metaList = arrayOf<NameValuePair>(
                        NameValuePair("fileName", file.originalFilename),
                        NameValuePair("fileLength", file.size.toString()),
                        NameValuePair("fileExt", ext),
                        NameValuePair("fileAuthor", author)
                )

                val fileLoadBean: FileLoadBean? = when(StringUtils.isNotBlank(groupName)){
                    true -> upload(groupName,fastDFSFile,metaList)
                    else -> upload(fastDFSFile,metaList)
                }
                fileLoadBean?.let {
                    val notNullBean = it
                    notNullBean.fileExt = ext
                    notNullBean.fileName = file.originalFilename
                    notNullBean.fileLength = file.size
                    fileService.let {
                        val insertFile = it.insertFile(fileLoadBean)
                        log.info("文件[{}]已经存入文件数据库中,对应ID为:{}", file.originalFilename, insertFile!!.toString())
                        insertFile.let {
                            notNullBean.fileId = it
                        }

                    }
                }
                fileLoadBean?.fileExt = ext
                fileLoadBean?.fileName = file.originalFilename
                fileLoadBean?.fileLength = file.size
                return fileLoadBean
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        return null
    }

    override fun upload(files: Array<MultipartFile>): List<FileLoadBean>? {
        val list = ArrayList<FileLoadBean>(files.size)
        for (multipartFile in files) {
            val upload = upload(multipartFile)
            if(upload != null){
                list.add(upload)
            }else{
                return null
            }
        }
        return list
    }

    override fun upload(groupName: String, files: Array<MultipartFile>): List<FileLoadBean>? {
        val list = ArrayList<FileLoadBean>(files.size)
        for (multipartFile in files) {
            val upload = upload(groupName, multipartFile)
            if(upload != null){
                list.add(upload)
            }else {
                return null
            }
        }
        return list
    }

    override fun download(fileId: String): ResponseEntity<ByteArray>? {
        fileId.let {
            val fileLoadBean = fileService?.selectFile(it)
             if (fileLoadBean != null) {
                return download(fileLoadBean.groupName, fileLoadBean.remoteFileName, fileLoadBean.fileName)
            }
        }
        return null
    }

    override fun downloadStream(fileId: String): InputStream? {
        val fileLoadBean = fileService.selectFile(fileId)
        return download(fileLoadBean!!.groupName, fileLoadBean.remoteFileName)
    }

    override fun update(fileId: String, file: MultipartFile): FileLoadBean? {
        delete(fileId)
        return upload(file)
    }

    override fun update(fileIds: Set<String>, files: Array<MultipartFile>): List<FileLoadBean>? {
        delete(fileIds)
        return upload(files)
    }

    override fun update(fileId: String, groupName: String, file: MultipartFile): FileLoadBean? {
        delete(fileId)
        return upload(groupName, file)
    }

    override fun update(fileIds: Set<String>, groupName: String, files: Array<MultipartFile>): List<FileLoadBean>? {
        delete(fileIds)
        return upload(groupName, files)
    }

    override fun delete(fileId: String): Int {
        val fileLoadBean = fileService.selectFile(fileId)
        if (fileLoadBean != null) {
            fileService.deleteFile(fileId)
            return delete(fileLoadBean.groupName, fileLoadBean.remoteFileName)
        }
        log.warn("删除文件ID:[{}],时没有在数据库中找到对应的数据", fileId)
        return 0
    }

    override fun delete(fileIds: Set<String>): Int {
        val n = fileIds.sumBy { delete(it) }
        return n
    }

    /**
     * 下载文件(返回InputStream)
     *
     * @param groupName      组名称
     * @param remoteFileName 文件名
     * @return InputStream对象
     */
    private fun download(groupName: String?, remoteFileName: String?): InputStream? {
        var content: ByteArray? = null
        try {
            val storageClient = init()
            content = storageClient.download_file(groupName, remoteFileName)
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: MyException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            connectionPool.checkin(trackerServer)
        }
        return if (content == null) null else ByteArrayInputStream(content)
    }

    /**
     * 下载文件
     *
     * @param groupName      文件所在组名称
     * @param remoteFileName 文件名称
     * @param specFileName   文件类型
     * @return ResponseEntity对象
     */
    private fun download(groupName: String?, remoteFileName: String?, specFileName: String?): ResponseEntity<ByteArray> {
        val headers = HttpHeaders()
        var content: ByteArray? = null
        try {
            val storageClient = init()
            content = storageClient.download_file(groupName, remoteFileName)
            headers.setContentDispositionFormData("attachment",
                    String(specFileName!!.toByteArray(charset("UTF-8")), ISO_8859_1))
            headers.contentType = MediaType.APPLICATION_OCTET_STREAM
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            connectionPool.checkin(trackerServer)
        }
        return ResponseEntity<ByteArray>(content, headers, HttpStatus.CREATED)
    }


    /**
     * 删除文件
     *
     * @param groupName      分组名称
     * @param remoteFileName 文件名称
     * @return 1成功, 0失败
     */

    fun delete(groupName: String?, remoteFileName: String?): Int {
        try {
            val storageClient = init()
            return storageClient.delete_file(groupName, remoteFileName)
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: MyException) {
            e.printStackTrace()
        } finally {
            connectionPool.checkin(trackerServer)
        }
        return 0
    }


    /**
     * 上传文件
     *
     * @param file       FastDFSFile文件
     * @param valuePairs 文件分卷信息
     * @return 文件路径
     */
    private fun upload(file: FastDFSFile, valuePairs: Array<NameValuePair>): FileLoadBean? {
        try {
            val storageClient = init()
            val uploadResults: Array<String>
            uploadResults = storageClient.upload_file(file.getContent(), file.getExt(), valuePairs)
            if (uploadResults.size > 0) {
                val groupName = uploadResults[0]
                val remoteFileName = uploadResults[1]
                return FileLoadBean(fastDfsService, groupName, remoteFileName)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            connectionPool.checkin(trackerServer)
        }
        return null
    }

    /**
     * 上传文件
     *
     * @param groupName
     * @param file
     * @param valuePairs
     * @return
     */
    private fun upload(groupName: String?, file: FastDFSFile, valuePairs: Array<NameValuePair>): FileLoadBean? {
        try {
            val storageClient = init()
            val uploadResults: Array<String>
            uploadResults = storageClient.upload_file(groupName, file.getContent(), file.getExt(), valuePairs)
            if (uploadResults.size > 0) {
                val remoteFileName = uploadResults[1]
                return FileLoadBean(fastDfsService, groupName, remoteFileName)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            connectionPool.checkin(trackerServer)
        }
        return null
    }


    /**
     * base64 上传图片
     *
     * @param fileBase64 图片base64码
     * @param valuePairs 文件分卷信息
     * @return 文件路径
     */
    private fun bufferUpload(fileBase64: String, valuePairs: Array<NameValuePair>): FileLoadBean? {
        try {
            val fileBuffer = Base64.getUrlEncoder().encode(fileBase64.toByteArray(charset("utf-8")))
            val storageClient = init()
            val uploadResults: Array<String>
            uploadResults = storageClient.upload_file(fileBuffer, 0, 0, "", valuePairs)
            if (uploadResults.size > 0) {
                val groupName = uploadResults[0]
                val remoteFileName = uploadResults[1]
                return FileLoadBean(fastDfsService, groupName, remoteFileName)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            connectionPool!!.checkin(trackerServer)
        }
        return null
    }


    companion object {

        private val log = LoggerFactory.getLogger(FastDFSService::class.java)

        private val storageServer: StorageServer? = null

        private var trackerServer: TrackerServer? = null
    }
}
