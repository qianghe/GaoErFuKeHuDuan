package com.aigao.golf.common.file.service.impl

import com.aigao.golf.common.file.FileLoadBean
import com.aigao.golf.common.file.model.CmSysFile
import com.aigao.golf.common.file.service.CmSysFileService
import com.aigao.golf.common.file.service.IWebFileService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.Date

/**
 * @author rankai
 * createTime 2017-10-2017/10/27 18:15
 */
@Service
class IWebFileServiceImpl : IWebFileService {

    @Autowired
    private lateinit var sysFileService: CmSysFileService

    override fun insertFile(fileLoadBean: FileLoadBean): String? {
        val cmSysFile = CmSysFile()
        cmSysFile.createTime = Date()
        cmSysFile.fileName = fileLoadBean.fileName
        cmSysFile.fileLength = fileLoadBean.fileLength.toString()
        cmSysFile.fileExt = fileLoadBean.fileExt
        cmSysFile.fileServer = fileLoadBean.fileServer
        cmSysFile.groupName = fileLoadBean.groupName
        cmSysFile.remoteFileName = fileLoadBean.remoteFileName
        sysFileService.insertSelective(cmSysFile)
        return cmSysFile.id
    }

    override fun selectFile(fileId: String?): FileLoadBean? {
        fileId?.let {
            val cmSysFile = sysFileService.selectByPrimaryKey(it)
            var loadBean: FileLoadBean? = null
            loadBean = FileLoadBean(cmSysFile.fileServer, cmSysFile.groupName, cmSysFile.remoteFileName)
            loadBean.fileId = cmSysFile.id
            loadBean.fileExt = cmSysFile.fileExt
            loadBean.fileLength = java.lang.Long.parseLong(cmSysFile.fileLength)
            loadBean.fileName = cmSysFile.fileName
            return loadBean
        }
        return null

    }

    override fun deleteFile(fileId: String?) {
        fileId?.let {
            sysFileService.deleteByPrimaryKey(it)
        }

    }
}
