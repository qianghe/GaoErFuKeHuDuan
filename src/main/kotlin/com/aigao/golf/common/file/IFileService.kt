package com.aigao.golf.common.file

/**
 * @author rankai
 * createTime 2017-10-2017/10/18 15:07
 */
interface IFileService {

    fun insertFile(fileLoadBean: FileLoadBean): String?

    fun selectFile(fileId: String?): FileLoadBean?

    fun deleteFile(fileId: String?)
}
