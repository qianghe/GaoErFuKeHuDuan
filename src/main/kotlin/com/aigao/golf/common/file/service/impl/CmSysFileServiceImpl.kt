package com.aigao.golf.common.file.service.impl

import com.aigao.golf.common.base.MyBaseServiceImpl
import com.aigao.golf.common.file.dao.CmSysFileMapper
import com.aigao.golf.common.file.model.CmSysFile
import com.aigao.golf.common.file.service.CmSysFileService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * @author rankai
 * createTime 2017-10-2017/10/27 17:04
 */
@Service
class CmSysFileServiceImpl : MyBaseServiceImpl<CmSysFile>(), CmSysFileService {

    @Autowired
    private lateinit var sysFileMapper: CmSysFileMapper

}
