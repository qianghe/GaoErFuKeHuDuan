package com.aigao.golf.common.file

class FileLoadBean(var fileServer: String?, var groupName: String?, var remoteFileName: String?) {
    var fileId: Any? = null
    var fileName: String? = null
    var fileExt: String? = null
    var fileLength: Long = 0
}
