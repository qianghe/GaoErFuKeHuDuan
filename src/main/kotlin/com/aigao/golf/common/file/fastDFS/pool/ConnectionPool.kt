package com.aigao.golf.common.file.fastDFS.pool

import org.apache.commons.lang3.StringUtils
import org.csource.common.MyException
import org.csource.fastdfs.ClientGlobal
import org.csource.fastdfs.ProtoCommon
import org.csource.fastdfs.TrackerClient
import org.csource.fastdfs.TrackerServer
import org.slf4j.LoggerFactory

import java.io.IOException
import java.util.Properties
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.TimeUnit

/**
 * @author rankai
 * createTime 2017-03-2017/3/6 14:15
 */
class ConnectionPool {

    var connectTimeoutInSeconds = DEF_CONNECT_TIMEOUT_IN_SECONDS
    var networkTimeoutInSeconds = DEF_NETWORK_TIMEOUT_IN_SECONDS
    var charset = DEF_CHARSET
    var httpAntiStealToken = DEF_HTTP_ANTI_STEAL_TOKEN
    var httpSecretKey = DEF_HTTP_SECRET_KEY
    var httpTrackerHttpPort = DEF_HTTP_TRACKER_HTTP_PORT
    var trackerServers: String? = null

    var minPoolSize = DEF_MIN_POOL_SIZE //连接池默认最小连接数
    var maxPoolSize = DEF_MAX_POOL_SIZE //连接池默认最大连接数
    var waitTimes = DEF_WAIT_TIMES //默认等待时间（单位：秒）
    var tryCount = DEF_TRY_COUNT //fastdfs客户端创建连接默认1次


    var idleConnectionPool: LinkedBlockingQueue<TrackerServer>? = null //空闲的连接池
    @Volatile private var nowPoolSize = 0 //当前创建的连接数

    private val beat: HeartBeat? = null

    @Throws(MyException::class)
    fun init() {
        if (StringUtils.isBlank(trackerServers)) {
            throw MyException("trackerServer参数为空")
        } else {
            properties.put("fastdfs.connect_timeout_in_seconds", connectTimeoutInSeconds)
            properties.put("fastdfs.network_timeout_in_seconds", networkTimeoutInSeconds)
            properties.put("fastdfs.charset", charset)
            properties.put("fastdfs.http_anti_steal_token", httpAntiStealToken)
            properties.put("fastdfs.http_secret_key", httpSecretKey)
            properties.put("fastdfs.http_tracker_http_port", httpTrackerHttpPort)
            properties.put("fastdfs.tracker_servers", trackerServers!!)
            poolInit()
        }
    }


    /**
     * @Description: 连接池初始化 (在加载当前ConnectionPool时执行) 1).加载配置文件 2).空闲连接池初始化；
     * 3).创建最小连接数的连接，并放入到空闲连接池；
     */
    private fun poolInit() {
        try {
            initClientGlobal()
            idleConnectionPool = LinkedBlockingQueue()
            for (i in 0 until minPoolSize) {
                createTrackerServer()
            }
            if (minPoolSize > idleConnectionPool!!.size) {
                log.error("初始化fastdfsPool数量异常", "实际pool数量为:[" + idleConnectionPool!!.size + "],应为[" + minPoolSize + "]")
            }
        } catch (e: Exception) {
            log.error("initClientGlobal执行失败")
            e.printStackTrace()
        }

    }

    /**
     * @Description: 创建TrackerServer, 并放入空闲连接池
     */
    private fun createTrackerServer() {
        var trackerServer: TrackerServer? = null
        val trackerClient = TrackerClient()
        try {
            trackerServer = trackerClient.connection
            var copyTryCount = tryCount
            while (trackerServer == null && copyTryCount != 0) {
                log.warn("获取trackerServer失败,正在进行第[$copyTryCount]次尝试")
                copyTryCount--
                initClientGlobal()
                trackerServer = trackerClient.connection
            }
            if (trackerServer != null) {
                if (ProtoCommon.activeTest(trackerServer.socket)) {
                    idleConnectionPool!!.add(trackerServer)
                    synchronized(this) {
                        nowPoolSize++
                    }
                }
            }
        } catch (e: IOException) {
            log.error("getConnection()失败")
            e.printStackTrace()
        } catch (e: Exception) {
            log.error("initClientGlobal执行失败")
            e.printStackTrace()
        } finally {
            if (trackerServer != null) {
                try {
                    trackerServer.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

        }
    }


    /**
     * @throws Exception
     * @Description: 获取空闲连接 1).在空闲池（idleConnectionPool)中弹出一个连接；
     * 2).把该连接放入忙碌池（busyConnectionPool）中; 3).返回 connection
     * 4).如果没有idle connection, 等待 wait_time秒, and check again
     */

    fun checkout(): TrackerServer? {
        var trackerServer: TrackerServer? = idleConnectionPool!!.poll()
        if (trackerServer == null) {
            if (nowPoolSize < maxPoolSize) {
                createTrackerServer()
                try {
                    trackerServer = idleConnectionPool!!.poll(waitTimes.toLong(), TimeUnit.SECONDS)
                } catch (e: Exception) {
                    log.error("连接池重启", "原因" + e.message)
                }

            }
        }
        return trackerServer
    }

    /**
     * @Description: 释放繁忙连接 1.如果空闲池的连接小于最小连接值，就把当前连接放入idleConnectionPool；
     * 2.如果空闲池的连接等于或大于最小连接值，就把当前释放连接丢弃；
     *
     *
     * 需释放的连接对象
     */

    fun checkin(trackerServer: TrackerServer?) {
        if (trackerServer != null) {
            if (idleConnectionPool!!.size < minPoolSize) {
                idleConnectionPool!!.add(trackerServer)
            } else {
                synchronized(this) {
                    if (nowPoolSize != 0) {
                        nowPoolSize--
                    }
                }
            }
        }
    }

    /**
     * @param trackerServer
     * @Description: 删除不可用的连接，并把当前连接数减一（调用过程中trackerServer报异常，调用一般在finally中）
     */
    fun drop(trackerServer: TrackerServer?) {
        if (trackerServer != null) {
            try {
                synchronized(this) {
                    if (nowPoolSize != 0) {
                        nowPoolSize--
                    }
                }
                trackerServer.close()
            } catch (e: IOException) {
                log.error("连接池删除无用连接", "原因" + e.message)
            }

        }
    }

    @Throws(Exception::class)
    private fun initClientGlobal() {
        ClientGlobal.initByProperties(properties)
    }



    companion object {

        private val log = LoggerFactory.getLogger(ConnectionPool::class.java)
        private val DEF_CONNECT_TIMEOUT_IN_SECONDS = 2
        private val DEF_NETWORK_TIMEOUT_IN_SECONDS = 30
        private val DEF_CHARSET = "utf-8"
        private val DEF_HTTP_ANTI_STEAL_TOKEN = "no"
        private val DEF_HTTP_SECRET_KEY = "FastDFS1234567890"
        private val DEF_HTTP_TRACKER_HTTP_PORT = 80
        private val DEF_TRY_COUNT = 3

        private val DEF_MIN_POOL_SIZE = 1
        private val DEF_MAX_POOL_SIZE = 20
        private val DEF_WAIT_TIMES = 1
        private val properties = Properties()
    }
}


