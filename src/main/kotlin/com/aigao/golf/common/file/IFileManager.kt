package com.aigao.golf.common.file

import org.springframework.http.ResponseEntity
import org.springframework.web.multipart.MultipartFile

import java.io.InputStream

/**
 * 文件上传下载统一接口
 *
 * @author rankai
 * createTime 2017-11-2017/11/2 14:21
 */
interface IFileManager {

    /**
     * 上传文件
     *
     * @param file 文件
     * @return
     */
    fun upload(file: MultipartFile): FileLoadBean?

    /**
     * 带组批量上传文件
     *
     * @param groupName
     * @param file
     * @return
     */
    fun upload(groupName: String?, file: MultipartFile): FileLoadBean?

    /**
     * 批量上传文件
     *
     * @param files
     * @return
     */
    fun upload(files: Array<MultipartFile>): List<FileLoadBean>?

    /**
     * 带组上传文件
     *
     * @param groupName
     * @param files
     * @return
     */
    fun upload(groupName: String, files: Array<MultipartFile>): List<FileLoadBean>?

    /**
     * 下载文件
     *
     * @param fileId
     * @return
     */
    fun download(fileId: String): ResponseEntity<ByteArray>?

    /**
     * 批量下载文件,返回输入流集合
     *
     * @param fileId
     * @return
     */
    fun downloadStream(fileId: String): InputStream?

    /**
     * 更新文件
     *
     * @param fileId
     * @param file
     * @return
     */
    fun update(fileId: String, file: MultipartFile): FileLoadBean?

    /**
     * 批量更新文件
     *
     * @param fileIds
     * @param files
     * @return
     */
    fun update(fileIds: Set<String>, files: Array<MultipartFile>): List<FileLoadBean>?

    /**
     * 带组更新文件
     *
     * @param fileId
     * @param groupName
     * @param file
     * @return
     */
    fun update(fileId: String, groupName: String, file: MultipartFile): FileLoadBean?

    /**
     * 带组批量更新文件
     *
     * @param fileIds
     * @param groupName
     * @param files
     * @return
     */
    fun update(fileIds: Set<String>, groupName: String, files: Array<MultipartFile>): List<FileLoadBean>?

    /**
     * 删除
     *
     * @param fileId
     * @return
     */
    fun delete(fileId: String): Int

    /**
     * 批量删除
     *
     * @param fileIds
     * @return
     */
    fun delete(fileIds: Set<String>): Int
}
