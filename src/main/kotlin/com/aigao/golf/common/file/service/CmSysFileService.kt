package com.aigao.golf.common.file.service


import com.aigao.golf.common.base.MyBaseService
import com.aigao.golf.common.file.model.CmSysFile

/**
 * @author rankai
 * createTime 2017-10-2017/10/27 17:02
 */
interface CmSysFileService : MyBaseService<CmSysFile> {
    companion object {

        fun buildUri(sysFile: CmSysFile): String {
            val stringBuffer = StringBuffer()
            stringBuffer
                    .append(sysFile.fileServer)
                    .append(sysFile.groupName)
                    .append("/")
                    .append(sysFile.remoteFileName)
            return stringBuffer.toString()
        }
    }
}
