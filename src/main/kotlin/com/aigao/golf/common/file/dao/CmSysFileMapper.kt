package com.aigao.golf.common.file.dao

import com.aigao.golf.common.base.MyBaseMapper
import com.aigao.golf.common.file.model.CmSysFile

interface CmSysFileMapper : MyBaseMapper<CmSysFile>