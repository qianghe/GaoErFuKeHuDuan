package com.aigao.golf.common.file.service


import com.aigao.golf.common.file.IFileService

/**
 * @author rankai
 * createTime 2017-10-2017/10/27 18:14
 */
interface IWebFileService : IFileService
