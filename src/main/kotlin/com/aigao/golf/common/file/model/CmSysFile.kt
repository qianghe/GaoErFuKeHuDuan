package com.aigao.golf.common.file.model


import com.aigao.golf.common.base.BaseModel
import javax.persistence.*
import java.util.Date

@Table(name = "CM_SYS_FILE")
class CmSysFile : BaseModel<String>() {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: String? = null

    @Column(name = "FILE_SERVER")
    var fileServer: String? = null

    @Column(name = "GROUP_NAME")
    var groupName: String? = null

    @Column(name = "REMOTE_FILE_NAME")
    var remoteFileName: String? = null

    @Column(name = "FILE_NAME")
    var fileName: String? = null

    @Column(name = "FILE_EXT")
    var fileExt: String? = null

    @Column(name = "FILE_LENGTH")
    var fileLength: String? = null

    @Column(name = "CREATE_TIME")
    var createTime: Date? = null

    @Transient
    var url: String? = null

}