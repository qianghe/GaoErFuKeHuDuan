package com.aigao.golf.common

object Rex{

    /**
     * email正则表达式(以验证)
     */
    val REGEX_EMAIL = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$"

    /**
     * 手机号正则表达式
     */
    val REGEX_PHONE = "^[1][3,4,5,7,8][0-9]{9}$"

    /**
     * 中文正则表达式
     */
    val REGEX_CHINESE = "[\\u4E00-\\u9FA5]+"

    /**
     * JSON正则表达式
     */
    val REGEX_JSON = "[^\\\"\\\\\\n\\r]*\\\"|true|false|null|-?\\\\d+(?:\\\\.\\\\d*)?(?:[eE][+\\\\-]?\\\\d+)?"

    /**
     * URL正则表达式
     */
    val REGEX_URL = "^((https|http|ftp|rtsp|mms)?:\\/\\/)[^\\s]+"

    /**
     * 用户名正则表达式
     */
    val REGEX_USERNAME = "[A-Za-z0-9_\\-\\u4e00-\\u9fa5]+"

    /**
     * IP正则表达式
     */
    val REGEX_IP = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|" + "[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)"


}