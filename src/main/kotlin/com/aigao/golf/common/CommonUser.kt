package com.aigao.golf.common

import java.util.Date

/**
 * Created by gelon on 2017/12/9.
 */
interface CommonUser {

    var id: String?

    var phone: String?

    var cname: String?

    var creater: String?

    var createTime: Date?

    var authorizationCode: String?

    var orgId: String?

}
