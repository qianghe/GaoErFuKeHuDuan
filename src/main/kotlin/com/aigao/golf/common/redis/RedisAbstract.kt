package com.aigao.golf.common.redis

import org.springframework.util.CollectionUtils
import java.util.concurrent.TimeUnit

/**
 * @author rankai
 * createTime 2017-09-2017/9/26 11:32
 */
abstract class RedisAbstract : RedisInterface {


//    abstract fun buildRedisTemplate(): RedisTemplate<String, Any>


//    /**
//     * 指定缓存失效时间
//     *
//     * @param key  键
//     * @param time 时间(秒)
//     * @return
//     */
//    override fun expire(key: String, time: Long): Boolean {
//        try {
//            if (time > 0) {
//                buildRedisTemplate().expire(key, time, TimeUnit.SECONDS)
//            }
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 根据key 获取过期时间
//     *
//     * @param key 键 不能为null
//     * @return 时间(秒) 返回0代表为永久有效
//     */
//    override fun getExpire(key: String): Long? {
//        return buildRedisTemplate().getExpire(key, TimeUnit.SECONDS)
//    }
//
//    /**
//     * 判断key是否存在
//     *
//     * @param key 键
//     * @return true 存在 false不存在
//     */
//    override fun hasKey(key: String): Boolean {
//        try {
//            return buildRedisTemplate().hasKey(key)!!
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 删除缓存
//     *
//     * @param key 可以传一个值 或多个
//     */
//    override fun delete(vararg key: String) {
//        if (key.isNotEmpty()) {
//            if (key.size == 1) {
//                buildRedisTemplate().delete(key[0])
//            } else {
//                buildRedisTemplate().delete(CollectionUtils.arrayToList(key).toMutableList() as MutableList<String>)
//            }
//        }
//    }
//
//
//    /**
//     * 普通缓存获取
//     *
//     * @param key 键
//     * @return 值
//     */
//    override fun get(key: String?): Any? {
//        return if (key == null) null else buildRedisTemplate().opsForValue().get(key)
//    }
//
//    /**
//     * 普通缓存放入
//     *
//     * @param key   键
//     * @param value 值
//     * @return true成功 false失败
//     */
//    override fun set(key: String, value: Any): Boolean {
//        try {
//            buildRedisTemplate().opsForValue().set(key, value)
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 普通缓存放入并设置时间
//     *
//     * @param key   键
//     * @param value 值
//     * @param time  时间(秒) time要大于0 如果time小于等于0 将设置无限期
//     * @return true成功 false 失败
//     */
//    override fun set(key: String, value: Any, time: Long): Boolean {
//        try {
//            if (time > 0) {
//                buildRedisTemplate().opsForValue().set(key, value, time, TimeUnit.SECONDS)
//            } else {
//                set(key, value)
//            }
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 递增
//     *
//     * @param key   键
//     * @param delta 要增加几(大于0)
//     * @return
//     */
//    override fun incr(key: String, delta: Long): Long {
//        if (delta < 0) {
//            throw RuntimeException("递增因子必须大于0")
//        }
//        return buildRedisTemplate().opsForValue().increment(key, delta)!!
//    }
//
//    /**
//     * 递减
//     *
//     * @param key   键
//     * @param delta 要减少几(小于0)
//     * @return
//     */
//    override fun decr(key: String, delta: Long): Long {
//        if (delta < 0) {
//            throw RuntimeException("递减因子必须大于0")
//        }
//        return buildRedisTemplate().opsForValue().increment(key, -delta)!!
//    }
//
//    //================================Map=================================
//
//    /**
//     * HashGet
//     *
//     * @param key  键 不能为null
//     * @param item 项 不能为null
//     * @return 值
//     */
//    override fun mapGet(key: String, item: String): Any {
//        return buildRedisTemplate().opsForHash<Any, Any>().get(key, item)
//    }
//
//    /**
//     * 获取hashKey对应的所有键值
//     *
//     * @param key 键
//     * @return 对应的多个键值
//     */
//    override fun mapGet(key: String): Map<Any, Any> {
//        return buildRedisTemplate().opsForHash<Any, Any>().entries(key)
//    }
//
//    /**
//     * HashSet
//     *
//     * @param key 键
//     * @param map 对应多个键值
//     * @return true 成功 false 失败
//     */
//    override fun mapSet(key: String, map: Map<String, Any>): Boolean {
//        try {
//            buildRedisTemplate().opsForHash<Any, Any>().putAll(key, map)
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * HashSet 并设置时间
//     *
//     * @param key  键
//     * @param map  对应多个键值
//     * @param time 时间(秒)
//     * @return true成功 false失败
//     */
//    override fun mapSet(key: String, map: Map<String, Any>, time: Long): Boolean {
//        try {
//            buildRedisTemplate().opsForHash<Any, Any>().putAll(key, map)
//            if (time > 0) {
//                expire(key, time)
//            }
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 向一张hash表中放入数据,如果不存在将创建
//     *
//     * @param key   键
//     * @param item  项
//     * @param value 值
//     * @return true 成功 false失败
//     */
//    override fun mapSet(key: String, item: String, value: Any): Boolean {
//        try {
//            buildRedisTemplate().opsForHash<Any, Any>().put(key, item, value)
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 向一张hash表中放入数据,如果不存在将创建
//     *
//     * @param key   键
//     * @param item  项
//     * @param value 值
//     * @param time  时间(秒)  注意:如果已存在的hash表有时间,这里将会替换原有的时间
//     * @return true 成功 false失败
//     */
//    override fun mapSet(key: String, item: String, value: Any, time: Long): Boolean {
//        try {
//            buildRedisTemplate().opsForHash<Any, Any>().put(key, item, value)
//            if (time > 0) {
//                expire(key, time)
//            }
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 删除hash表中的值
//     *
//     * @param key  键 不能为null
//     * @param item 项 可以使多个 不能为null
//     */
//    override fun mapDelete(key: String, vararg item: Any) {
//        buildRedisTemplate().opsForHash<Any, Any>().delete(key, *item)
//    }
//
//    /**
//     * 判断hash表中是否有该项的值
//     *
//     * @param key  键 不能为null
//     * @param item 项 不能为null
//     * @return true 存在 false不存在
//     */
//    override fun mapHasKey(key: String, item: String): Boolean {
//        return buildRedisTemplate().opsForHash<Any, Any>().hasKey(key, item)!!
//    }
//
//    /**
//     * hash递增 如果不存在,就会创建一个 并把新增后的值返回
//     *
//     * @param key  键
//     * @param item 项
//     * @param by   要增加几(大于0)
//     * @return
//     */
//    override fun mapIncr(key: String, item: String, by: Double): Double {
//        return buildRedisTemplate().opsForHash<Any, Any>().increment(key, item, by)!!
//    }
//
//    /**
//     * hash递减
//     *
//     * @param key  键
//     * @param item 项
//     * @param by   要减少记(小于0)
//     * @return
//     */
//    override fun mapDecr(key: String, item: String, by: Double): Double {
//        return buildRedisTemplate().opsForHash<Any, Any>().increment(key, item, -by)!!
//    }
//
//    //============================set=============================
//
//    /**
//     * 根据key获取Set中的所有值
//     *
//     * @param key 键
//     * @return
//     */
//    override fun setGet(key: String): Set<Any>? {
//        try {
//            return buildRedisTemplate().opsForSet().members(key)
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return null
//        }
//
//    }
//
//    /**
//     * 根据value从一个set中查询,是否存在
//     *
//     * @param key   键
//     * @param value 值
//     * @return true 存在 false不存在
//     */
//    override fun setHasKey(key: String, value: Any): Boolean {
//        try {
//            return buildRedisTemplate().opsForSet().isMember(key, value)!!
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 将数据放入set缓存
//     *
//     * @param key    键
//     * @param values 值 可以是多个
//     * @return 成功个数
//     */
//    override fun setSet(key: String, vararg values: Any): Long {
//        try {
//            return buildRedisTemplate().opsForSet().add(key, *values)!!
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return 0
//        }
//
//    }
//
//    /**
//     * 将set数据放入缓存
//     *
//     * @param key    键
//     * @param time   时间(秒)
//     * @param values 值 可以是多个
//     * @return 成功个数
//     */
//    override fun setSetAndTime(key: String, time: Long, vararg values: Any): Long {
//        try {
//            val count = buildRedisTemplate().opsForSet().add(key, *values)
//            if (time > 0) expire(key, time)
//            return count!!
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return 0
//        }
//
//    }
//
//    /**
//     * 获取set缓存的长度
//     *
//     * @param key 键
//     * @return
//     */
//    override fun setGetSetSize(key: String): Long {
//        try {
//            return buildRedisTemplate().opsForSet().size(key)!!
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return 0
//        }
//
//    }
//
//    /**
//     * 移除值为value的
//     *
//     * @param key    键
//     * @param values 值 可以是多个
//     * @return 移除的个数
//     */
//    override fun setRemove(key: String, vararg values: Any): Long {
//        try {
//            val count = buildRedisTemplate().opsForSet().remove(key, *values)
//            return count!!
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return 0
//        }
//
//    }
//    //===============================list=================================
//
//    /**
//     * 获取list缓存的内容
//     *
//     * @param key   键
//     * @param start 开始
//     * @param end   结束  0 到 -1代表所有值
//     * @return
//     */
//    override fun listGet(key: String, start: Long, end: Long): List<Any>? {
//        try {
//            return buildRedisTemplate().opsForList().range(key, start, end)
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return null
//        }
//
//    }
//
//    /**
//     * 获取list缓存的长度
//     *
//     * @param key 键
//     * @return
//     */
//    override fun listGetListSize(key: String): Long {
//        try {
//            return buildRedisTemplate().opsForList().size(key)!!
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return 0
//        }
//
//    }
//
//    /**
//     * 通过索引 获取list中的值
//     *
//     * @param key   键
//     * @param index 索引  index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
//     * @return
//     */
//    override fun listGetIndex(key: String, index: Long): Any? {
//        try {
//            return buildRedisTemplate().opsForList().index(key, index)
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return null
//        }
//
//    }
//
//    /**
//     * 将list放入缓存
//     *
//     * @param key   键
//     * @param value 值
//     * @return
//     */
//    override fun listSet(key: String, value: Any): Boolean {
//        try {
//            buildRedisTemplate().opsForList().rightPush(key, value)
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 将list放入缓存
//     *
//     * @param key   键
//     * @param value 值
//     * @param time  时间(秒)
//     * @return
//     */
//    override fun listSet(key: String, value: Any, time: Long): Boolean {
//        try {
//            buildRedisTemplate().opsForList().rightPush(key, value)
//            if (time > 0) expire(key, time)
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 将list放入缓存
//     *
//     * @param key   键
//     * @param value 值
//     * @return
//     */
//    override fun listSet(key: String, value: List<Any>): Boolean {
//        try {
//            buildRedisTemplate().opsForList().rightPushAll(key, arrayOf(value))
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 将list放入缓存
//     *
//     * @param key   键
//     * @param value 值
//     * @param time  时间(秒)
//     * @return
//     */
//    override fun listSet(key: String, value: List<Any>, time: Long): Boolean {
//        try {
//            buildRedisTemplate().opsForList().rightPushAll(key, arrayOf(value))
//            if (time > 0) expire(key, time)
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 根据索引修改list中的某条数据
//     *
//     * @param key   键
//     * @param index 索引
//     * @param value 值
//     * @return
//     */
//    override fun listUpdateIndex(key: String, index: Long, value: Any): Boolean {
//        try {
//            buildRedisTemplate().opsForList().set(key, index, value)
//            return true
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return false
//        }
//
//    }
//
//    /**
//     * 移除N个值为value
//     *
//     * @param key   键
//     * @param count 移除多少个
//     * @param value 值
//     * @return 移除的个数
//     */
//    override fun listRemove(key: String, count: Long, value: Any): Long {
//        try {
//            val remove = buildRedisTemplate().opsForList().remove(key, count, value)
//            return remove!!
//        } catch (e: Exception) {
//            e.printStackTrace()
//            return 0
//        }
//
//    }
}
