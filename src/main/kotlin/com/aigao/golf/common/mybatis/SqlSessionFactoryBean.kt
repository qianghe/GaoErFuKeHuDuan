package com.aigao.golf.common.mybatis

import org.apache.commons.lang3.ArrayUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.core.io.support.ResourcePatternResolver

import java.io.IOException

/**
 * @author rankai
 * createTime 2017-09-2017/9/27 15:42
 */
class SqlSessionFactoryBean : org.mybatis.spring.SqlSessionFactoryBean() {

    var resourceLoader: ResourcePatternResolver? = null

    @Throws(IOException::class)
    fun setStrMapperLocations(strMapperLocations: String) {
        var resourcesAll = arrayOfNulls<Resource>(0)
        val split = strMapperLocations.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        for (i in split.indices) {
            val resourcesTemp = resourceLoader?.getResources(split[i])
            resourcesAll = ArrayUtils.addAll<Resource>(resourcesAll, *resourcesTemp)
        }
        super.setMapperLocations(resourcesAll)
    }


}
