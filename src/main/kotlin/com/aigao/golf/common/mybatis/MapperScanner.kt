package com.aigao.golf.common.mybatis


import com.aigao.golf.common.base.BaseModel
import com.aigao.golf.common.base.MyBaseMapper
import com.aigao.golf.common.config.JdbcConfig.Companion.MYSQL_DEV
import com.aigao.golf.common.config.JdbcConfig.Companion.MYSQL_PRO
import com.aigao.golf.common.config.JdbcConfig.Companion.ORACLE_DEV
import com.aigao.golf.common.config.JdbcConfig.Companion.ORACLE_PRO

import java.util.Properties

/**
 * @author rankai
 * createTime 2017-09-2017/9/27 16:39
 */
class MapperScanner : tk.mybatis.spring.mapper.MapperScannerConfigurer() {

    private var profile: Int? = null

    init {
        println("mapperScanner constructor")
    }

    fun buildProperties() {
        println("mapperScanner buildProperties")
        super.setMarkerInterface(SUPER_CLASS)
        val properties = Properties()
        properties.setProperty("mapper", "tk.mybatis.mapper.common.Mapper")
        properties.setProperty("ORDER", "BEFORE")
        if (profile == ORACLE_DEV || profile == ORACLE_PRO) {
            properties.setProperty("IDENTITY", ORACLE_IDENTITY)
        } else if (profile == MYSQL_DEV || profile == MYSQL_PRO) {
            properties.setProperty("IDENTITY", MYSQL_IDENTITY)
        }
        super.setProperties(properties)
    }

    fun setProfile(profile: Int) {
        this.profile = profile
    }

    override fun setMarkerInterface(superClass: Class<*>) {
    }

    companion object {

        private val ORACLE_IDENTITY = "SELECT UUID FROM DUAL"

        private val MYSQL_IDENTITY = "SELECT UUID()"

        private val SUPER_CLASS = MyBaseMapper::class.java
    }


}
