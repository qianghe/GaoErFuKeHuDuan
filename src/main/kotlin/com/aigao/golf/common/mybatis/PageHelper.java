package com.aigao.golf.common.mybatis;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.util.Map;
import java.util.Properties;

/**
 * @author rankai
 *         createTime 2017-09-2017/9/27 15:56
 */
@Intercepts(@Signature(type = Executor.class, method = "query",
    args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}))
public class PageHelper extends com.github.pagehelper.PageHelper {

    private static final String DIALECT_NAME = "dialect";

    private String dialect;

    private Map<String, String> mapProperties;

    public void setDialect(String dialect) {
        this.dialect = dialect;
    }

    public void setMapProperties(Map<String, String> mapProperties) {
        this.mapProperties = mapProperties;
    }

    public void buildProperties() {
        Properties properties = new Properties();
        for (Map.Entry<String, String> entry : mapProperties.entrySet()) {
            properties.put(entry.getKey(), entry.getValue());
        }
        properties.put(DIALECT_NAME, dialect);
        super.setProperties(properties);
    }
}
