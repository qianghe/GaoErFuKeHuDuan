package com.aigao.golf.common.shiro

import com.aigao.golf.common.CommonUser
import com.aigao.golf.common.permissions.PermissionChecked
import com.aigao.golf.management.user.service.GlUserService
import com.aigao.golf.market.customer.model.MkCustomer
import com.aigao.golf.market.customer.service.MkCustomerService
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authz.AuthorizationInfo
import org.apache.shiro.authz.Authorizer
import org.apache.shiro.authz.SimpleAuthorizationInfo
import org.apache.shiro.realm.AuthorizingRealm
import org.apache.shiro.session.Session
import org.apache.shiro.subject.PrincipalCollection
import org.apache.shiro.subject.Subject
import org.apache.tomcat.util.security.PermissionCheck
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired

import javax.annotation.PostConstruct

abstract class BaseAuthRealm : AuthorizingRealm() {
    @Autowired
    lateinit var mkCustomerService: MkCustomerService
    @Autowired
    lateinit var glUserService: GlUserService


    private val log = LoggerFactory.getLogger(BaseAuthRealm::class.java)


    /**
     * 授权
     */
    override fun doGetAuthorizationInfo(principals: PrincipalCollection): AuthorizationInfo? {
        ShiroKit.getPrincipal()?.let {
            if(it is CommonUser){
                val simpleAuthorInfo = SimpleAuthorizationInfo()
                simpleAuthorInfo.addStringPermission(it.authorizationCode)
                return simpleAuthorInfo
            }
        }
        return null
    }


//    override fun isPermitted(principals: PrincipalCollection, permission: String): Boolean {
//        var commonUser = principals.primaryPrincipal as CommonUser
//        return PermissionChecked.passAll(commonUser.authorizationCode,permission)
//    }



    private fun setSession(user: CommonUser) {
        val currentUser = SecurityUtils.getSubject()
        if (currentUser != null) {
            val session = currentUser.session
            session?.setAttribute("user", user)
        }
    }

    @PostConstruct
    open fun initCredentialsMatcher() {
        this.credentialsMatcher = UserCredentialsMatcherPwd()
    }



}
