package com.aigao.golf.common.shiro;

import com.aigao.golf.common.CommonUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

public class ShiroKit {
    public static final String SHIRO_REDIS_KEY = "shiro.redis.";
    public static final String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|_|\\\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\\\.)+[a-zA-Z]{2,}$";
    public static final String REGEX_PHONE = "^[1][3,4,5,7,8][0-9]{9}$";

    public ShiroKit() {
    }

    public static Object getPrincipal() {
        return SecurityUtils.getSubject().getPrincipal();
    }

    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    public static Session getSession() {
        return getSubject().getSession();
    }

    public static void login(AuthenticationToken token) {
        getSubject().login(token);
    }

    public static void logout() {
        getSubject().logout();
    }

    public static CommonUser getUser() {
        return (CommonUser)getPrincipal();
    }

    public static String getOrgId(){
        return ((CommonUser)getPrincipal()).getOrgId();
    }

    public static CommonUser getShiroModel() {
        return (CommonUser) getPrincipal();
    }

}
