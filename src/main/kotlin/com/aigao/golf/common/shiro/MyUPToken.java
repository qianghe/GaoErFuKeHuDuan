package com.aigao.golf.common.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * Created by gelon on 2017/12/9.
 */
public class MyUPToken extends UsernamePasswordToken {

    /**
     * 渠道
     */
    int channel ;


    /**
     * 客户小程序
     */
    public static final int MK_LITE_APP = 1;

    /**
     * 工作人员web
     */
    public static final int GL_WEB = 2;

    public MyUPToken(String username , String password, int channel){
        super(username,password);
        this.channel = channel ;
    }

    public int getChannel() {
        return channel;
    }

    public MyUPToken setChannel(int channel) {
        this.channel = channel;
        return this;
    }
}
