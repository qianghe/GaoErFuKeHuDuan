package com.aigao.golf.common.shiro

import com.aigao.golf.Utils.PasswordUtil
import com.aigao.golf.management.user.model.GlUser
import com.aigao.golf.market.customer.model.MkCustomer
import org.apache.shiro.authc.AuthenticationInfo
import org.apache.shiro.authc.AuthenticationToken
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher

class UserCredentialsMatcherPwd : SimpleCredentialsMatcher() {

    override fun doCredentialsMatch(authcToken: AuthenticationToken, info: AuthenticationInfo): Boolean {
        val token = authcToken as MyUPToken
        when (token.getChannel()) {
            MyUPToken.MK_LITE_APP -> {
                val customer = getCredentials(info) as MkCustomer
                return customer.password == PasswordUtil.encryptPassword(String(token.password), customer.salt)
            }
            MyUPToken.GL_WEB -> {
                val glUser = getCredentials(info) as GlUser
                return glUser.password == PasswordUtil.encryptPassword(String(token.password), glUser.salt)
            }
            else -> return false
        }

    }
}
