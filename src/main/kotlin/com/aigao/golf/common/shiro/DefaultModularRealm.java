package com.aigao.golf.common.shiro;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.realm.Realm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;

public class DefaultModularRealm extends ModularRealmAuthenticator {
    private static final Logger log = LoggerFactory.getLogger(DefaultModularRealm.class);
    private Map<Integer, Realm> realms;

    public DefaultModularRealm() {
    }

    protected AuthenticationInfo doMultiRealmAuthentication(Collection<Realm> realms, AuthenticationToken token) {
        return super.doMultiRealmAuthentication(realms, token);
    }

    protected AuthenticationInfo doAuthenticate(AuthenticationToken authenticationToken) throws AuthenticationException {
        this.assertRealmsConfigured();
        Realm realm = null;
        MyUPToken token = (MyUPToken)authenticationToken;
        switch (token.getChannel()){
            case MyUPToken.GL_WEB:
            case MyUPToken.MK_LITE_APP:
            default:
                realm = realms.get(token.getChannel());
        }
        if(realm == null) {
            realm = realms.get(MyUPToken.GL_WEB);
        }
        return this.doSingleRealmAuthentication(realm, authenticationToken);
    }


    public void setRealms(Map<Integer, Realm> realms) {
        this.realms = realms;
    }
}
