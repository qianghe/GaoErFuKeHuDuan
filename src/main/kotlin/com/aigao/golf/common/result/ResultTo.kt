package com.aigao.golf.common.result

import java.io.Serializable
import java.util.HashMap

/**
 * 返回结果
 *
 * @author rankai
 * createTime 2017-04-2017/4/20 14:47
 */
class ResultTo : HashMap<String, Any>, Serializable {

    constructor() {
        this.put(RESULT_CODE, ResultEnum.SUCCESS.code)
        this.put(RESULT_MESSAGE, ResultEnum.SUCCESS.message)
    }

    constructor(any: Any) {
        if (any is ResultEnum) {
            this.put(RESULT_CODE, any.code)
            this.put(RESULT_MESSAGE, any.message)
        } else {
            this.put(RESULT_CODE, ResultEnum.SUCCESS.code)
            this.put(RESULT_MESSAGE, ResultEnum.SUCCESS.message)
            this.put(RESULT_DATA, any)
        }
    }

    constructor(resultEnum: ResultEnum, message: String) {
        this.put(RESULT_CODE, resultEnum.code)
        this.put(RESULT_MESSAGE, message)
    }

    constructor(code: Int, message: String) {
        this.put(RESULT_CODE, code)
        this.put(RESULT_MESSAGE, message)
    }

    fun setData(`object`: Any): ResultTo {
        this.put(RESULT_DATA, `object`)
        return this
    }

    fun setData(dataName: String, `object`: Any): ResultTo {
        this.put(dataName, `object`)
        return this
    }

    fun setData(map: Map<String, Any>): ResultTo {
        this.putAll(map)
        return this
    }

    fun setPerms(`object`: Any) {
        this.put(RESULT_PERMS, `object`)
    }

    companion object {

        private const val serialVersionUID = -6125210369527938613L

        // 返回结果编码
        private val RESULT_CODE = "code"
        // 返回结果信息
        private val RESULT_MESSAGE = "message"
        // 返回结果数据
        private val RESULT_DATA = "data"

        private val RESULT_PERMS = "perms"
    }
}
