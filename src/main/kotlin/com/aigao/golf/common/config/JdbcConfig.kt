package com.aigao.golf.common.config

import com.aigao.golf.common.mybatis.MapperScanner
import com.aigao.golf.common.mybatis.PageHelper
import com.aigao.golf.common.mybatis.SqlSessionFactoryBean
import com.alibaba.druid.pool.DruidDataSource
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import org.springframework.jdbc.datasource.DataSourceTransactionManager

/**
 * 数据库连接配置
 */
@Configuration
class JdbcConfig{

    companion object {
        val MYSQL_DEV = 1
        val MYSQL_PRO = 2
        val ORACLE_DEV = 3
        val ORACLE_PRO = 4
        val profile = MYSQL_DEV
    }



    @Bean(name = ["dataSource"],initMethod = "init",destroyMethod = "close")
    fun getDataSource():DruidDataSource{
        val druidDS = DruidDataSource()
        druidDS.url = "jdbc:mysql://192.168.1.115:3306/golf?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&useSSL=false"
        druidDS.driverClassName = "com.mysql.jdbc.Driver"
        druidDS.username = "root"
        druidDS.password = "qkjr_420"
        druidDS.initialSize = 20
        druidDS.minIdle = 1
        druidDS.maxActive = 300
        druidDS.maxWait = 60000
        druidDS.timeBetweenEvictionRunsMillis = 60000
        druidDS.minEvictableIdleTimeMillis = 300000
        druidDS.validationQuery = "SELECT 'x'"
        druidDS.isTestWhileIdle = true
        druidDS.isTestOnBorrow = false
        druidDS.isTestOnReturn = false
        druidDS.isPoolPreparedStatements = true
        druidDS.maxPoolPreparedStatementPerConnectionSize = 20
        druidDS.setFilters("stat")
        return druidDS
    }


    @Bean(name = ["pageHelper"],initMethod = "buildProperties")
    fun pageHelper(): PageHelper {
        val pageHelper = PageHelper()
        pageHelper.setDialect("mysql")
        pageHelper.setMapProperties(mutableMapOf(
                "rowBoundsWithCount" to "true",
                "reasonable" to "true",
                "offsetAsPageNum" to "true"
        ))
        return pageHelper
    }


    @Bean(name = ["mybatisConfig"])
    fun mybatisConfig(): org.apache.ibatis.session.Configuration {
        var config = org.apache.ibatis.session.Configuration()
        config.isMapUnderscoreToCamelCase = true
        return config
    }


    @Bean(name = ["sqlSessionFactory"])
    fun sqlSessionFactory(@Qualifier("dataSource") dataSource: DruidDataSource,
                          @Qualifier("pageHelper") pageHelper: PageHelper,
                          @Qualifier("mybatisConfig") mybatisConfig: org.apache.ibatis.session.Configuration): SqlSessionFactoryBean{
        val sqlSF = SqlSessionFactoryBean()
        sqlSF.setDataSource(dataSource)
        sqlSF.resourceLoader = PathMatchingResourcePatternResolver()
        sqlSF.setStrMapperLocations("classpath*:/base/res/mapper/mysql/**/*Mapper.xml,classpath:/mappers/mysql/**/*Mapper.xml")
        sqlSF.setPlugins(arrayOf(pageHelper))
        sqlSF.setConfiguration(mybatisConfig)
        return sqlSF
    }


    @Bean(name = ["transactionManager"])
    fun transactionManager(@Qualifier("dataSource") dataSource: DruidDataSource): DataSourceTransactionManager {
        val dsTransManager = DataSourceTransactionManager()
        dsTransManager.dataSource = dataSource
        return dsTransManager
    }

    @Bean(initMethod = "buildProperties")
    fun mapperScanner():MapperScanner{
        val mapperScan = MapperScanner()
        mapperScan.setProfile(profile)
        mapperScan.setBasePackage("com.aigao.golf")
        return mapperScan
    }


}
