package com.aigao.golf.common.config

import com.aigao.golf.common.file.fastDFS.FastDFSService
import com.aigao.golf.common.file.service.impl.IWebFileServiceImpl
import com.aigao.golf.common.file.fastDFS.pool.ConnectionPool
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * 文件配置
 */
@Configuration
class FileConfig{

    @Bean(name = ["connectionPool"],initMethod = "init")
    fun connectionPool(): ConnectionPool {
        val pool = ConnectionPool()
        pool.connectTimeoutInSeconds = 2
        pool.networkTimeoutInSeconds = 30
        pool.charset = "utf-8"
        pool.httpTrackerHttpPort = 80
        pool.httpAntiStealToken = "no"
        pool.httpSecretKey = "FastDFS1234567890"
        pool.minPoolSize = 3
        pool.maxPoolSize = 15
        pool.tryCount = 3
        pool.waitTimes = 1
        pool.trackerServers = "192.168.1.117:22122"
        return pool
    }


    @Bean(name = ["fileServiceImpl"])
    fun fileServiceImpl() = IWebFileServiceImpl()


    @Bean
    fun fastDFSService(@Qualifier("connectionPool")connectionPool:ConnectionPool,
                       @Qualifier("fileServiceImpl")fileServiceImpl:IWebFileServiceImpl):FastDFSService{
        val fastDfsService = FastDFSService()
        fastDfsService.connectionPool = connectionPool
        fastDfsService.author = "qkjr"
        fastDfsService.fastDfsService = "http://sxpg.qk.in/"
        fastDfsService.fileService = fileServiceImpl
        return fastDfsService
    }


}
