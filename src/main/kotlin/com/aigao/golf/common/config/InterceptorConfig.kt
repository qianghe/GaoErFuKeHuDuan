package com.aigao.golf.common.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter

@Configuration
class InterceptorConfig : WebMvcConfigurerAdapter(){

    override fun addInterceptors(registry: InterceptorRegistry?) {
        registry?.addInterceptor(PreControllerFilter())
        super.addInterceptors(registry)
    }

}
