package com.aigao.golf.common.config

import com.aigao.golf.common.permissions.PermissionChecked
import com.aigao.golf.common.permissions.ReqPermission
import com.aigao.golf.common.shiro.ShiroKit
import org.springframework.web.method.HandlerMethod
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class PreControllerFilter : HandlerInterceptorAdapter(){

    /**
     * 拦截请求进行自定义权限判断
     * 如果方法没有权限要求直接通过
     * 否则进行权限审查
     */
    override fun preHandle(request: HttpServletRequest?, response: HttpServletResponse?, handler: Any?): Boolean {
        if(handler is HandlerMethod){
            val permissionAnno: ReqPermission = handler.getMethodAnnotation(ReqPermission::class.java) ?: return true
            if(PermissionChecked.pass(ShiroKit.getUser().authorizationCode,permissionAnno.type,permissionAnno.opt)){
                return true
            }
            return false
        }
        return super.preHandle(request, response, handler)
    }

}

