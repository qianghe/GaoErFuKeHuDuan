package com.aigao.golf.common.permissions


/**
 * 权限声明  type 代表声明的权限类型  opt 代表操作
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
annotation class ReqPermission(val type: PermissionType , val opt : Array<PermissOpt>)
