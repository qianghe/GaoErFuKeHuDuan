package com.aigao.golf.common.permissions

/**
 * 权限检查类
 */
object PermissionChecked{


    fun pass(userCode: String?,type: PermissionType, opts : Array<PermissOpt>) : Boolean{
        userCode?.let {
            val askCode = opts.sumBy { it.code }
            val userPosPermission = userCode.substring(type.code,type.code + 1).toInt(16)
            return askCode and userPosPermission == askCode
        }
        return false
    }



}
