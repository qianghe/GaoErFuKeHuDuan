package com.aigao.golf.common.permissions

/**
 * 权限的基本操作 增删改查
 */
enum class PermissOpt constructor(internal var code: Int) {

    READ(1), WRITE(1 shl  1), UPDATE(1 shl 2), DELETE(1 shl 3)
}
