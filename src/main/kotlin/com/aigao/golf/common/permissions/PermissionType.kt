package com.aigao.golf.common.permissions

/**
 * 所有的权限声明 所有的权限都必须在此进行声明
 */
enum class PermissionType constructor(var code : Int,var title : String){

    /**
     * 我的信息
     */
    MY_INFO(0,"我的信息"),
    /**
     * 组信息
     */
    GROUP_INFO(1,"组信息");



}
