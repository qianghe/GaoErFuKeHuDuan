package com.aigao.golf.common.datatable

/**
 * datetable参数
 *
 * @author rankai
 * createTime 2017-01-2017/1/12 15:40
 */
object TableCode {

    val PAGE_START_NAME = "pageStart"

    val PAGE_LENGTH_NAME = "pageLength"

    val PAGE_ORDER_NAME = "pageOrder"
}
