package com.aigao.golf.common.datatable;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author rankai
 *         createTime 2017-09-2017/9/26 11:26
 */
public final class TableMethod {

    private static final Map<String, Method> map = new HashMap<>();

    public static void push(String str, Method method) {
        if (get(str) != null) {
            try {
                throw new TableException("DataTable名称[" + str + "]已存在");
            } catch (TableException e) {
                e.printStackTrace();
            }
        }
        map.put(str, method);
    }

    public static Method get(String str) {
        return map.get(str);
    }
}
