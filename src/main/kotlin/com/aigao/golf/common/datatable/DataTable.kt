package com.aigao.golf.common.datatable

import java.lang.annotation.*

/**
 * @author rankai
 * createTime 2017-09-2017/9/26 11:36
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
@Documented
annotation class DataTable(val name: String = "")
