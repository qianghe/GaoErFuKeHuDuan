package com.aigao.golf.common.datatable;

/**
 * @author rankai
 *         createTime 2017-09-2017/9/26 11:32
 */
public class TableException extends Exception {

    public TableException(String message) {
        super(message);
    }
}
