package com.aigao.golf.common.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbsLogger {

    protected Logger log = LoggerFactory.getLogger(AbsLogger.class);

    public AbsLogger() {
        if (buildLogger() != null) {
            log = buildLogger();
        }
    }

    protected abstract Logger buildLogger();

}
