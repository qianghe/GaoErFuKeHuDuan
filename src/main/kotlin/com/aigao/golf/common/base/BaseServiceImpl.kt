package com.aigao.golf.common.base

import com.aigao.golf.common.datatable.TableMethod
import com.aigao.golf.common.log.AbsLogger
import com.aigao.golf.common.datatable.DataTable
import com.github.pagehelper.PageHelper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import tk.mybatis.mapper.common.BaseMapper
import tk.mybatis.mapper.entity.Example

import java.lang.reflect.Method

/**
 * 用户控制层
 *
 * @author rankai
 * Timecreate 2016-12-2016/12/21 10:22
 */
abstract class BaseServiceImpl<T : BaseModel<String>> : AbsLogger(), MyBaseService<T> {
    init {
        val clz = this.javaClass
        val methods = clz.declaredMethods
        for (i in methods.indices) {
            if (methods[i].getAnnotation(DataTable::class.java) != null) {
                val name = methods[i].getAnnotation(DataTable::class.java).name
                baseLog.debug("find dateTable methods:[" + methods[i].declaringClass.name + "." + methods[i].name + "] identifying:[" + name + "]")
                TableMethod.push(name, methods[i])
            }
        }
    }

    override fun buildLogger(): Logger {
        return LoggerFactory.getLogger(this.javaClass)
    }

    protected abstract fun buildDao(): MyBaseMapper<T>

    override fun selectByPrimaryKey(id: Any): T {
        return buildDao().selectByPrimaryKey(id)
    }

    override fun selectOne(`object`: T): T {
        return buildDao().selectOne(`object`)
    }

    override fun select(`object`: T): List<T> {
        return buildDao().select(`object`)
    }

    override fun selectAll(): List<T> {
        return buildDao().selectAll()
    }

    override fun selectCount(`object`: T): Int {
        return buildDao().selectCount(`object`)
    }

    override fun selectCountByExample(example: Example): Int {
        return buildDao().selectCountByExample(example)
    }

    override fun selectByExample(example: Example): List<T> {
        return buildDao().selectByExample(example)
    }

    override fun delete(`object`: T): Int {
        return buildDao().delete(`object`)
    }

    override fun deleteByExample(example: Example): Int {
        return buildDao().deleteByExample(example)
    }

    override fun deleteByPrimaryKey(id: Any): Int {
        return buildDao().deleteByPrimaryKey(id)
    }

    override fun insert(`object`: T): Int {
        return buildDao().insert(`object`)
    }

    //    @Override
    //    public int insertList(List<T> objects) {
    //        return buildDao().insertList(objects);
    //    }

    override fun insertSelective(`object`: T): Int {
        return buildDao().insertSelective(`object`)
    }

    //    @Override
    //    public int insertUseGeneratedKeys(T object) {
    //        return buildDao().insertUseGeneratedKeys(object);
    //    }

    override fun updateByExample(`object`: T, example: Example): Int {
        return buildDao().updateByExample(`object`, example)
    }

    override fun updateByExampleSelective(`object`: T, example: Example): Int {
        return buildDao().updateByExampleSelective(`object`, example)
    }

    override fun updateByPrimaryKey(`object`: T): Int {
        return buildDao().updateByPrimaryKey(`object`)
    }

    override fun updateByPrimaryKeySelective(`object`: T): Int {
        return buildDao().updateByPrimaryKeySelective(`object`)
    }

    override fun pageList(record: T, page: Int?, size: Int?): List<T> {
        PageHelper.startPage<Any>(page!!, size!!)
        return buildDao().select(record)
    }

    override fun pageList(example: Example, page: Int?, size: Int?): List<T> {
        PageHelper.startPage<Any>(page!!, size!!)
        return buildDao().selectByExample(example)
    }

    companion object {

        private val baseLog = LoggerFactory.getLogger(BaseServiceImpl::class.java)
    }
}
