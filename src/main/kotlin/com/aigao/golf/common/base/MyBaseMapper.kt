package com.aigao.golf.common.base


import tk.mybatis.mapper.common.Mapper

/**
 * MyBaseMapper
 *
 * @author rankai
 * createTime 2017-04-2017/4/20 17:14
 */

interface MyBaseMapper<T : BaseModel<String>> : Mapper<T>
