package com.aigao.golf.common.base

import com.aigao.golf.common.file.FileLoadBean
import com.aigao.golf.common.file.IFileManager
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.web.multipart.MultipartFile

import java.util.Arrays
import java.util.HashMap
import java.util.function.BiFunction
import java.util.stream.Collectors


/**
 * @author chenkai
 * createTime 2017-10-2017/10/16 20:33
 */
open class MyBaseServiceImpl<T : BaseModel<String>> : BaseServiceImpl<T>(), MyBaseService<T> {

    @Autowired
    private lateinit var baseDao: MyBaseMapper<T>

    @Autowired
    @Lazy
    protected lateinit var fileManager: IFileManager


    override fun buildDao(): MyBaseMapper<T> {
        return baseDao
    }

    override fun buildLogger(): Logger {
        return LoggerFactory.getLogger(this.javaClass)
    }



}
