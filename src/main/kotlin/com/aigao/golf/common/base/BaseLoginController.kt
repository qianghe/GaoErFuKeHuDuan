package com.aigao.golf.common.base

import com.aigao.golf.common.shiro.MyUPToken
import com.aigao.golf.common.result.ResultEnum
import com.aigao.golf.common.result.ResultTo
import com.aigao.golf.common.shiro.ShiroKit
import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.*

/**
 * Created by gelon on 2017/12/9.
 */
open class BaseLoginController<T: BaseModel<String>> : BaseController<T>() {

    /**
     * 通用验证登录
     */
    protected fun validateLogin(token: MyUPToken): ResultTo {
        token.isRememberMe = true
        val currentUser = SecurityUtils.getSubject()
        try {
            currentUser.login(token)
        } catch (var6: UnknownAccountException) {
            return ResultTo(ResultEnum.OPERATION_FAILED, "未知的用户名[${token.username}]")
        } catch (var7: IncorrectCredentialsException) {
            return ResultTo(ResultEnum.OPERATION_FAILED, "验证不通过")
        } catch (var8: LockedAccountException) {
            return ResultTo(ResultEnum.OPERATION_FAILED, "对用户[ ${token.username} ]进行登录验证..验证未通过,账户已锁定")
        } catch (var9: ExcessiveAttemptsException) {
            return ResultTo(ResultEnum.OPERATION_FAILED, "对用户[ ${token.username} ]进行登录验证..验证未通过,错误次数过多")
        } catch (var10: AuthenticationException) {
            return ResultTo(ResultEnum.OPERATION_FAILED, "对用户[ ${token.username} ]进行登录验证..验证未通过")
        }

        return if (currentUser.isAuthenticated) ResultTo().setData(ShiroKit.getUser()) else ResultTo(ResultEnum.OPERATION_FAILED, "登陆失败")
    }


}
