package com.aigao.golf.common.base

import tk.mybatis.mapper.entity.Example

/**
 * @author rankai
 * createTime 2017-03-2017/3/10 15:00
 */
interface MyBaseService<T : BaseModel<String>> {

    /**
     * 根据主键字段进行查询，方法参数必须包含完整的主键属性，查询条件使用等号
     *
     * @param id 主键
     * @return 实体
     */
    fun selectByPrimaryKey(id: Any): T

    /**
     * 根据实体中的属性进行查询，只能有一个返回值，有多个结果是抛出异常，查询条件使用等号
     *
     * @param object 实体
     * @return 满足条件实体
     */
    fun selectOne(`object`: T): T

    /**
     * 根据实体中的属性值进行查询，查询条件使用等号
     *
     * @param object 实体
     * @return 实体列表
     */
    fun select(`object`: T): List<T>

    /**
     * 查询全部结果，select(null)方法能达到同样的效果
     *
     * @return 实体列表
     */
    fun selectAll(): List<T>

    /**
     * 根据实体中的属性查询总数，查询条件使用等号
     *
     * @param object 实体
     * @return 满足条件个数
     */
    fun selectCount(`object`: T): Int

    /**
     * 根据Example对象查询总数，查询条件使用等号
     *
     * @param example Example对象
     * @return 满足条件个数
     */
    fun selectCountByExample(example: Example): Int

    /**
     * 根据Example条件进行查询 这个查询支持通过Example类指定查询列，通过selectProperties方法指定查询列
     *
     * @param example Example对象
     * @return 实体信息list
     */
    fun selectByExample(example: Example): List<T>

    //    /**
    //     * @param object
    //     * @param rowBounds
    //     * @return
    //     */
    //    List<T> selectByRowBounds(T object, RowBounds rowBounds);
    //
    //    /**
    //     * @param example
    //     * @param rowBounds
    //     * @return
    //     */
    //    List<T> selectByExampleAndRowBounds(Example example, RowBounds rowBounds);

    /**
     * 根据实体属性作为条件进行删除，查询条件使用等号
     *
     * @param object 实体
     * @return 影响数据库行数
     */
    fun delete(`object`: T): Int

    /**
     * 根据example删除数据
     *
     * @param example Example对象
     * @return 影响数据库行数
     */
    fun deleteByExample(example: Example): Int

    /**
     * 根据主键字段进行删除，方法参数必须包含完整的主键属性
     *
     * @param id 主键
     * @return 影响数据库行数
     */
    fun deleteByPrimaryKey(id: Any): Int

    /**
     * 保存一个实体，null的属性也会保存，不会使用数据库默认值
     *
     * @param object 实体
     * @return 影响数据库行数
     */
    fun insert(`object`: T): Int

    /**
     * 批量插入数据
     *
     * @param objects 对象集合
     * @return 影响数据库行数
     */
    //    int insertList(List<T> objects);

    /**
     * 保存一个实体，null的属性不会保存，会使用数据库默认值
     *
     * @param object 实体
     * @return 影响数据库行数
     */
    fun insertSelective(`object`: T): Int

    /**
     * 根据主键更新实体全部字段，null值会被更新
     *
     * @param object 实体
     * @return 影响数据库行数
     */
    //    int insertUseGeneratedKeys(T object);

    /**
     * 根据example更新实体,null值会被更新
     *
     * @param object  实体
     * @param example Example对象
     * @return 影响数据库行数
     */
    fun updateByExample(`object`: T, example: Example): Int

    /**
     * 根据example更新属性不为null的值
     *
     * @param object  实体
     * @param example Example对象
     * @return 影响数据库行数
     */
    fun updateByExampleSelective(`object`: T, example: Example): Int

    /**
     * 根据主键更新实体全部字段，null值会被更新
     *
     * @param object 实体
     * @return 影响数据库行数
     */
    fun updateByPrimaryKey(`object`: T): Int

    /**
     * 根据主键更新属性不为null的值
     *
     * @param object 实体
     * @return 影响数据库行数
     */
    fun updateByPrimaryKeySelective(`object`: T): Int

    /**
     * 根据实体属性和RowBounds进行分页查询,以page作为开始参数
     *
     * @param object 实体
     * @param page   页数
     * @param size   个数
     * @return 分页实体个数
     */
    fun pageList(`object`: T, page: Int?, size: Int?): List<T>

    /**
     * 据实体属性和RowBounds进行分页查询,以page作为开始参数(基于example)
     *
     * @param example Examples示例
     * @param page    实体
     * @param size    个数
     * @return 分页实体个数
     */
    fun pageList(example: Example, page: Int?, size: Int?): List<T>

}
