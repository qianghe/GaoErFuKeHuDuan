package com.aigao.golf.common.base

import java.io.Serializable

/**
 * BaseModel
 *
 * @author rankai
 * createTime 2017-04-2017/4/20 15:38
 */
abstract class BaseModel<PK> : Serializable
