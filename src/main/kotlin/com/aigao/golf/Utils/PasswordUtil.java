package com.aigao.golf.Utils;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.StringUtils;

import java.util.UUID;

public class PasswordUtil {
    private static final String HASH_NAME = "md5";
    private static final int HASH_ITERATIONS = 128;

    public PasswordUtil() {
    }

    public static String encryptPassword(String password, String salt) {
        if(StringUtils.hasText(password) && StringUtils.hasText(salt)) {
            Md5Hash saltByte = new Md5Hash(salt);
            return (new SimpleHash("md5", password, saltByte, 128)).toString();
        } else {
            return "";
        }
    }

    public static String buildSalt() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
