package com.aigao.golf.market.customer.controller

import com.aigao.golf.Utils.PasswordUtil
import com.aigao.golf.common.base.BaseLoginController
import com.aigao.golf.common.permissions.PermissionType
import com.aigao.golf.common.permissions.PermissOpt
import com.aigao.golf.common.permissions.ReqPermission
import com.aigao.golf.common.result.ResultEnum
import com.aigao.golf.common.result.ResultTo
import com.aigao.golf.common.shiro.MyUPToken
import com.aigao.golf.common.shiro.ShiroKit
import com.aigao.golf.market.customer.service.MkCustomerService
import com.aigao.golf.market.customer.model.MkCustomer
import java.util.Date
import java.util.UUID
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.ArrayList
import java.util.HashMap

@RestController
@RequestMapping("/app/customer")
class CustomerController :BaseLoginController<MkCustomer>() {

    @Autowired
    lateinit var mkCustomerService: MkCustomerService


    @PostMapping("/login")
    fun login(username: String, password: String): ResultTo {
        val token = MyUPToken(username, password, MyUPToken.MK_LITE_APP)
        return validateLogin(token)
    }


    @PostMapping("/reg")
    fun reg(mkCustomer: MkCustomer): ResultTo {
        val salt = UUID.randomUUID().toString()
        mkCustomer.password = PasswordUtil.encryptPassword(mkCustomer.password, salt)
        mkCustomer.salt = salt
        mkCustomer.orgId = "null_temp"
        mkCustomer.createTime = Date()
        return if (mkCustomerService.insert(mkCustomer) == 1) ResultTo().setData(mkCustomer) else ResultTo(ResultEnum.OPERATION_FAILED, "保存失败")
    }


    @PostMapping("/permissionTest")
    @ReqPermission(type = PermissionType.MY_INFO, opt = [PermissOpt.WRITE,PermissOpt.DELETE])
    fun permission():ResultTo{
        return ResultTo(ShiroKit.getUser())
    }

    @PostMapping("/insertTest")
    fun insertTest():ResultTo{
        val salt = UUID.randomUUID().toString()
        var mkCustomer = MkCustomer()
        mkCustomer.password = PasswordUtil.encryptPassword(mkCustomer.password, salt)
        mkCustomer.salt = salt
        mkCustomer.orgId = "null_temp"
        mkCustomer.createTime = Date()
        mkCustomer.phone = "1231231232222"
        mkCustomer.cname = "王五"
        mkCustomer.authorizationCode = "FFFF"
        mkCustomerService.insert(mkCustomer)
        return ResultTo(mkCustomer)
    }


    @PostMapping("/customers")
    fun customers(id: String?): ResultTo {
        val map = HashMap<String, Any>()
        map.put("ids", "123")
        return dataPage(mkCustomerService, MkCustomer::class.java, "appCustomerExt", map)?: ResultTo(ResultEnum.OPERATION_FAILED,"空")
    }





}