package com.aigao.golf.market.customer.dao

import com.aigao.golf.common.base.MyBaseMapper
import com.aigao.golf.market.customer.model.MkCustomer

interface MkCustomerMapper : MyBaseMapper<MkCustomer>
