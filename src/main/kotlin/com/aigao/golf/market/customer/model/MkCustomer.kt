package com.aigao.golf.market.customer.model

import com.aigao.golf.common.CommonUser
import com.aigao.golf.common.base.BaseModel
import com.alibaba.fastjson.annotation.JSONField
import java.util.Date
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Table(name = "MK_CUSTOMER")
class MkCustomer : BaseModel<String>(),CommonUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: String? = null

    /**
     * 手机号
     */
    override var phone: String? = null

    /**
     * 密码
     */
    @JSONField(serialize = false)
    var password: String? = null

    /**
     * 名字
     */
    override var cname: String? = null

    /**
     * 创建者id
     */
    override var creater: String? = null

    /**
     * 创建时间
     */
    override var createTime: Date? = null

    /**
     * 授权码
     */
    override var authorizationCode: String? = null
    /**
     * 盐值
     */
    @JSONField(serialize = false)
    var salt: String? = null
    /**
     * 机构
     */
    override var orgId: String? = null

}
