package com.aigao.golf.market.customer.controller

import com.aigao.golf.common.result.ResultTo
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/customermore")
class CustomerMoreController {


    @PostMapping("/test")
    fun test(): ResultTo {
        return ResultTo()
    }

}
