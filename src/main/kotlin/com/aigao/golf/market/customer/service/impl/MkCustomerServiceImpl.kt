package com.aigao.golf.market.customer.service.impl

import com.aigao.golf.common.base.MyBaseMapper
import com.aigao.golf.common.base.MyBaseServiceImpl
import com.aigao.golf.common.datatable.DataTable
import com.aigao.golf.market.customer.dao.MkCustomerMapper
import com.aigao.golf.market.customer.model.MkCustomer
import com.aigao.golf.market.customer.service.MkCustomerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MkCustomerServiceImpl : MyBaseServiceImpl<MkCustomer>(),MkCustomerService {

    @Autowired
    private lateinit var mkCustomerMapper: MkCustomerMapper


    @DataTable("appCustomerExt")
    fun appCustomerExt(val map: HashMap<String,Any>) = mkCustomerMapper.selectByPrimaryKey(map.get("Id"))

}
